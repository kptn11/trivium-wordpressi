<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Architects+Daughter&display=swap" rel="stylesheet">
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Description et chaine alimentaire de l'animal">
    <title><?php the_title(); ?></title>
    <link rel="stylesheet" href="<?php echo esc_url( get_stylesheet_uri() ); ?>" type="text/css" />
    <?php wp_head(); ?>
</head>
<body>
<?php get_header('header.php'); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="animalCont">
    <div class="nomTailleDescri">
        <h3><?php the_title(); ?></h3>
        <h3>Taille : <?= get_field('taille') ?> toise</h3>
        <div class="description"> <?php the_content(); ?> </div>
    </div>
    <div class="imgMange">
        <img class="animalPNG" src="<?=  get_field("image")?>" alt="Photo de l'animal">
        <ul class="mange">Mange : <?php $manges = get_field("mange");
            foreach($manges as $mange){?>
            <a alt="Lien vers le repas" href="<?php echo get_the_permalink($mange);?>"><li><?php echo get_the_title($mange);?></li></a>
            <?php } ?>
        </ul>
    </div>
</div>


<?php endwhile; ?>

<?php
if ( get_next_posts_link() ) {
next_posts_link();
}
?>
<?php
if ( get_previous_posts_link() ) {
previous_posts_link();
}
?>

<?php else: ?>

<p>No posts found. :(</p>

<?php endif; ?>

</body>
</html>