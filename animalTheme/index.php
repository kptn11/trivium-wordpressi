<!DOCTYPE html>
<html lang="fr">
<head>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Architects+Daughter&display=swap" rel="stylesheet">
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Liste des animaux">
<title>INDEX</title>
<link rel="stylesheet" href="<?php echo esc_url( get_stylesheet_uri() ); ?>" type="text/css" />
<?php wp_head(); ?>
</head>
<body>
<?php get_header('header.php'); ?>
<?php 
    $args = array(
        'order' => 'ASC', // ASC ou DESC 
        'orderby' => 'title', // title, date, comment_count…
    );
    $query = new WP_Query( $args );
?>
<div class="animalList">
    <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
    <div class="card">
        <h2 id="name"><a alt="Lien de l'animal" href="<?= get_permalink(); ?>"><?= the_title(); ?></a></h2>
        <a href="<?= get_permalink(); ?>"><img alt="Image de l'animal" class="indexIMG" src="<?=  get_field("image")?>"></a>
    </div>
    <?php endwhile; ?>
</div>
<?php
if ( get_next_posts_link() ) {
next_posts_link();
}
?>
<?php
if ( get_previous_posts_link() ) {
previous_posts_link();
}
?>

<?php else: ?>

<p>No posts found. :(</p>

<?php endif; ?>

</body>
</html>